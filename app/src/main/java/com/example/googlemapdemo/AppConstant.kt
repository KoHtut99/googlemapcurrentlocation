package com.example.googlemapdemo

class AppConstant {
    companion object {
        @JvmStatic
        val STORAGE_REQUEST_CODE = 1000

        @JvmStatic
        val LOCAL_REQUEST_CODE = 2000

        @JvmStatic
        val PROFILE_PATH = "/Profile/image-profile.jpg"
    }
}